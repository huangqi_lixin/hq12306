package com.chepiao.houduan.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.chepiao.houduan.bean.BusAndRailway;

public interface BusAndRailwayMapper extends BaseMapper<BusAndRailway> {
}
