package com.chepiao.houduan.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.chepiao.houduan.bean.User;

public interface UserMapper extends BaseMapper<User> {
}
