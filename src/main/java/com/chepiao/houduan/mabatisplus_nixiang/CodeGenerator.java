package com.chepiao.houduan.mabatisplus_nixiang;

import com.baomidou.mybatisplus.generator.FastAutoGenerator;
import com.baomidou.mybatisplus.generator.config.OutputFile;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;
import org.junit.Test;

import java.util.Collections;


public class CodeGenerator {

    @Test
    public void run() {
        FastAutoGenerator.create("jdbc:mysql:///mp?serverTimezone=Asia/Shanghai&useSSL=false", "root", "12345678")
                .globalConfig(builder -> {
                    builder.author("huangqi") // 设置作者
                            .fileOverride() // 覆盖已生成文件
                            .outputDir("E:\\GZ-2201\\third\\springboot\\SpringBootProject\\mpdemo\\src\\main\\java"); // 指定输出目录
                })
                .packageConfig(builder -> {
                    builder.parent("com.huangqi.mpdemo") // 设置父包名
                            //                    .moduleName("system") // 设置父包模块名
                            .pathInfo(Collections.singletonMap(OutputFile.xml, "E:\\GZ-2201\\third\\springboot\\SpringBootProject\\mpdemo\\src\\main\\resources\\mapper")); // 设置mapperXml生成路径
                })
                .strategyConfig(builder -> {
                    builder.addInclude("account","author","book","user") // 设置需要生成的表名
                            .addTablePrefix("t_", "c_"); // 设置过滤表前缀
                })
                .templateEngine(new FreemarkerTemplateEngine()) // 使用Freemarker引擎模板，默认的是Velocity引擎模板
                .execute();

    }
}
