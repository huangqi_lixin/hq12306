package com.chepiao.houduan.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.chepiao.houduan.bean.BusAndRailway;
import com.chepiao.houduan.config.Myexception;
import com.chepiao.houduan.mapper.BusAndRailwayMapper;
import com.chepiao.houduan.service.BusAndRailService;
import org.springframework.stereotype.Service;

import javax.xml.crypto.Data;
import java.util.List;

@Service
public class BusAndRailwayServiceImp extends ServiceImpl<BusAndRailwayMapper, BusAndRailway> implements BusAndRailService {

    @Override
    public void insertCars(BusAndRailway busAndRailway) {
        baseMapper.insert(busAndRailway);
    }

    //时间查询
    @Override
    public List<BusAndRailway> getBusAndRailByDay(Data day) {
        QueryWrapper<BusAndRailway> wrapper = new QueryWrapper<>();
        wrapper.eq("date",day.toString());
        List<BusAndRailway> busAndRailwaysList = baseMapper.selectList(wrapper);
        return busAndRailwaysList;
    }

    //目的地查询
    @Override
    public List<BusAndRailway> getBusAndRailByName(String name) {
        QueryWrapper<BusAndRailway> wrapper = new QueryWrapper<>();
        wrapper.eq("destination",name);
        List<BusAndRailway> busAndRailwaysList = baseMapper.selectList(wrapper);
        return busAndRailwaysList;
    }

    //模糊查询
    @Override
    public List<BusAndRailway> fuzzyQuery(String name) {
        QueryWrapper<BusAndRailway> wrapper = new QueryWrapper<>();
        wrapper.like("destination",name);
        List<BusAndRailway> busAndRailwaysList = baseMapper.selectList(wrapper);
        return busAndRailwaysList;
    }
    //查询全部
    @Override
    public List<BusAndRailway> getAll() {
        List<BusAndRailway> busAndRailwaysList = baseMapper.selectList(null);
        return busAndRailwaysList;
    }
    //查询全部汽车
    @Override
    public List<BusAndRailway> getAllBus() {
        QueryWrapper<BusAndRailway> queryWrapper=new QueryWrapper<>();
        queryWrapper.eq("typecar","汽车");
        List<BusAndRailway> busAndRailwaysList = baseMapper.selectList(queryWrapper);
        return busAndRailwaysList;
    }
    //查询全部火车
    @Override
    public List<BusAndRailway> getAllRailway() {
        QueryWrapper<BusAndRailway> queryWrapper=new QueryWrapper<>();
        queryWrapper.eq("typecar","火车");
        List<BusAndRailway> busAndRailwaysList = baseMapper.selectList(queryWrapper);
        return busAndRailwaysList;
    }
    //查询全部高铁
    @Override
    public List<BusAndRailway> getAllSubway() {
        QueryWrapper<BusAndRailway> queryWrapper=new QueryWrapper<>();
        queryWrapper.eq("typecar","高铁");
        List<BusAndRailway> busAndRailwaysList = baseMapper.selectList(queryWrapper);
        return busAndRailwaysList;
    }

    @Override
    public void updataByid(BusAndRailway busAndRailway) {
        int i = baseMapper.updateById(busAndRailway);
        if(i==0){
            throw new Myexception(20001,"修改车票失败");
        }
    }

    @Override
    public List<BusAndRailway> getDengJiSubway(Integer dengji) {
        String dengJi=new String();
        if (dengji == 1) {
            dengJi="一等座";
        }
        else if (dengji == 2){
            dengJi="二等座";
        }
        else if (dengji == 3){
            dengJi="三等座";
        }
        QueryWrapper<BusAndRailway> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("dengji",dengJi);
        queryWrapper.eq("typecar","高铁");
        List<BusAndRailway> busAndRailways = baseMapper.selectList(queryWrapper);
        return busAndRailways;
    }

    @Override
    public List<BusAndRailway> getDengJiRailway(Integer dengji) {
        String dengJi=new String();
        if (dengji == 1) {
            dengJi="一等座";
        }
        else if (dengji == 2){
            dengJi="二等座";
        }else if (dengji == 3){
            dengJi="三等座";
        }
        QueryWrapper<BusAndRailway> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("dengji",dengJi);
        queryWrapper.eq("typecar","火车");
        List<BusAndRailway> busAndRailways = baseMapper.selectList(queryWrapper);
        return busAndRailways;
    }
}
