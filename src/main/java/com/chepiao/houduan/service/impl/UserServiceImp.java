package com.chepiao.houduan.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.chepiao.houduan.bean.User;
import com.chepiao.houduan.bean.UserRegist;
import com.chepiao.houduan.config.Myexception;
import com.chepiao.houduan.mapper.UserMapper;
import com.chepiao.houduan.service.UserService;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@Service
public class UserServiceImp  extends ServiceImpl<UserMapper,User>   implements UserService {

    //登录的方法
    @Override
    public int login( User member) {
        //获取登录手机号和密码
        String mobile = member.getTelephone();
        String password = member.getPassword();

        //手机号和密码非空判断
        if(StringUtils.isEmpty(mobile) || StringUtils.isEmpty(password)) {
        return 3000;
        }

        //判断手机号是否正确d2
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        wrapper.eq("telephone",mobile);
        User user = baseMapper.selectOne(wrapper);
        //判断查询对象是否为空
        if(user == null) {//没有这个手机号
            return  1000;
        }

        if(!password.equals(user.getPassword()))
        {
            return 2000;
        }
        return 200;
    }

    @Override
    public void register(UserRegist userRegist) {
        String code = userRegist.getCode(); //验证码
        String mobile = userRegist.getMobile(); //手机号
        String nickname = userRegist.getNickname(); //昵称
        String password = userRegist.getPassword(); //密码
        Integer sex=userRegist.getSex();//性别

        //非空判断
        if(StringUtils.isEmpty(mobile) || StringUtils.isEmpty(password)
                || StringUtils.isEmpty(code) || StringUtils.isEmpty(nickname)) {
            throw new Myexception(20001,"注册失败");
        }
        //自己设置的验证码，阿里云申请不了短信功能 123456
        String realcode="123456";
        if(!code.equals(realcode)) {
            throw new Myexception(20001,"注册失败");
        }
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        wrapper.eq("telephone",mobile);
        Integer count = baseMapper.selectCount(wrapper);
        if(count > 0) {
            throw new Myexception(20001,"注册失败");
        }
        User user = new User();
        user.setTelephone(mobile);
        user.setUsername(nickname);
        user.setPassword(password);
        user.setSex(sex);
        baseMapper.insert(user);
    }

    @Override
    public User getUser(String telephone) {
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        wrapper.eq("telephone",telephone);
        User user = baseMapper.selectOne(wrapper);
        return user;
    }
}
