package com.chepiao.houduan.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.chepiao.houduan.bean.BusAndRailway;

import javax.xml.crypto.Data;
import java.util.List;

public interface BusAndRailService extends IService<BusAndRailway> {
    //插入车辆数据
    public void insertCars(BusAndRailway busAndRailway);
    //查询通过日期
    public List<BusAndRailway> getBusAndRailByDay(Data day);
    //查询通过地名
    public List<BusAndRailway> getBusAndRailByName(String name);
    //模糊查询
    public List<BusAndRailway> fuzzyQuery(String name);
    //查询全部车
    public List<BusAndRailway> getAll();
    //查询全部汽车
    public List<BusAndRailway> getAllBus();
    //查询全部火车
    public List<BusAndRailway> getAllRailway();
    //查询全部高铁
    public List<BusAndRailway> getAllSubway();
    //通过id更新
    public void updataByid(BusAndRailway busAndRailway);
    //按车次等级查询全部地铁
    public List<BusAndRailway> getDengJiSubway(Integer dengji);

    public List<BusAndRailway> getDengJiRailway(Integer dengji);
}
