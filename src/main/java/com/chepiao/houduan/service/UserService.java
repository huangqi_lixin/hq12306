package com.chepiao.houduan.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.chepiao.houduan.bean.User;
import com.chepiao.houduan.bean.UserRegist;
import org.springframework.stereotype.Service;

import java.util.List;

public interface UserService extends IService<User> {
    //登录的方法
    int login(User user);

    //注册的方法
    void register(UserRegist userRegist);

    //回显用户
    User getUser(String telephone);

}
