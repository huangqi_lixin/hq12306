package com.chepiao.houduan.controller;

import com.chepiao.houduan.bean.R;
import com.chepiao.houduan.bean.User;
import com.chepiao.houduan.bean.UserRegist;
import com.chepiao.houduan.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@Api(tags = "用户组册和登录模块")
@RestController
@CrossOrigin
@RequestMapping("/user")
public class UserLogin {

    @Autowired
    UserService userService;

    //登录
    @ApiOperation("用户登录")
    @PostMapping("login")
    public R loginUser( @RequestBody User member) {
        //member对象封装手机号和密码
        //调用service方法实现登录
        int token = userService.login(member);
        return  R.ok().data("token",token);
    }

    //注册
    @ApiOperation("用户注册")
    @PostMapping("register")
    public R registerUser( @RequestBody UserRegist userRegist) {
        userService.register(userRegist);
        return R.ok();
    }
    //查询用户
    @ApiOperation("回显数据")
    @GetMapping("returnUser")
    public R returnUser(@RequestParam String telephone) {
        User user = userService.getUser(telephone);
        return R.ok().data("user",user);
    }

}
