package com.chepiao.houduan.controller;


import com.chepiao.houduan.bean.BusAndRailway;
import com.chepiao.houduan.bean.R;
import com.chepiao.houduan.service.BusAndRailService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.xml.crypto.Data;
import java.util.List;

@Api(tags = "查询车票信息和购买模块")
@RestController
@CrossOrigin
@RequestMapping("/busandrailway")
public class BusAndRainwayCon {

    @Autowired
    BusAndRailService busAndRailService;
    @GetMapping("test")
    public R test1(){
        return R.ok().data("good","good");
    }

    @ApiOperation("插入数据")
    @PostMapping("insertCars")
    public R insertBusAndRailway(@RequestBody BusAndRailway busAndRailway){
        busAndRailService.insertCars(busAndRailway);
        return R.ok();
    }


    @ApiOperation("时间查询")
    @GetMapping("getBusAndRailByDay")
    public R byDay(@RequestParam Data day){
        List<BusAndRailway> busAndRailByDay = busAndRailService.getBusAndRailByDay(day);
        return  R.ok().data("busAndRailByDay",busAndRailByDay);
    }

    @ApiOperation("地点查询")
    @GetMapping("getBusAndRailByName")
    public  R byName(@RequestParam String name){
        List<BusAndRailway> busAndRailByDay = busAndRailService.getBusAndRailByName(name);
        return  R.ok().data("busAndRailByDay",busAndRailByDay);
    }

    @ApiOperation("模糊查询")
    @GetMapping("fuzzyQuery")
    public  R byFuzzyName(@RequestParam String name){
        List<BusAndRailway> busAndRailByDay = busAndRailService.fuzzyQuery(name);
        return  R.ok().data("busAndRailByDay",busAndRailByDay);
    }

    @ApiOperation("查询全部")
    @GetMapping("getAll")
    public  R byGetAll(){
        List<BusAndRailway> busAndRailByDay = busAndRailService.getAll();
        return  R.ok().data("busAndRailByDay",busAndRailByDay);
    }
    @ApiOperation("查询全部汽车")
    @GetMapping("getAllBus")
    public  R byGetAllBus(){
        List<BusAndRailway> busAndRailByDay = busAndRailService.getAllBus();
        return  R.ok().data("busAndRailByDay",busAndRailByDay);
    }
    @ApiOperation("查询全部火车")
    @GetMapping("getAllRailway")
    public  R byGetAllRailway(){
        List<BusAndRailway> busAndRailByDay = busAndRailService.getAllRailway();
        return  R.ok().data("busAndRailByDay",busAndRailByDay);
    }
    @ApiOperation("查询全部高铁")
    @GetMapping("getAllSubway")
    public  R byGetAllSubway(){
        List<BusAndRailway> busAndRailByDay = busAndRailService.getAllSubway();
        return  R.ok().data("busAndRailByDay",busAndRailByDay);
    }

    @ApiOperation("按车位等次查询全部高铁")
    @GetMapping("getDengJiAllSubway")
    public  R byDengJiGetAllSubway(@RequestParam Integer dengji){
        List<BusAndRailway> busAndRailByDay = busAndRailService.getDengJiSubway(dengji);
        return  R.ok().data("busAndRailByDay",busAndRailByDay);
    }

    @ApiOperation("按车位等次查询全部火车")
    @GetMapping("getDengJiAllRailway")
    public  R byDengJiGetAllRailway(@RequestParam Integer dengji){
        List<BusAndRailway> busAndRailByDay = busAndRailService.getDengJiRailway(dengji);
        return  R.ok().data("busAndRailByDay",busAndRailByDay);
    }

    @ApiOperation("修改信息")
    @PostMapping("updateBus")
    public R updateBusAndRailway(@RequestBody BusAndRailway busAndRailway){
        busAndRailService.updataByid(busAndRailway);
        return R.ok();
    }

}
