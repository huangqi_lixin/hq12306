package com.chepiao.houduan.bean;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

@Data
@TableName("cars")
public class BusAndRailway {
    @TableId(type = IdType.AUTO)
    @ApiModelProperty(value = "id")
    private Long Id;

    @ApiModelProperty(value = "车次")
    private String carOrder;

    @ApiModelProperty(value = "出发地")
    private String initially;

    @ApiModelProperty(value = "目的地")
    private String destination;

    @ApiModelProperty(value = "价格")
    private BigDecimal price;

    @ApiModelProperty(value = "数量")
    private Integer quantity;

    @ApiModelProperty(value = "时间")
    private String date;

    @ApiModelProperty(value = "出发时间")
    private String departureTime;

    @ApiModelProperty(value = "预估路途时间")
    private String estimatedTime;

    @ApiModelProperty(value = "车辆类型 1、汽车，2、火车，3、高铁")
    private String typecar;

    @ApiModelProperty(value = "车座等级 一等、二等，3、三等")
    private String dengji;

}
