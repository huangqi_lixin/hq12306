package com.chepiao.houduan.bean;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

@Data
@TableName("user")
public class User {


    @ApiModelProperty(value = "手机号")
    private String telephone;

    @ApiModelProperty(value = "密码")
    private String password;

    @ApiModelProperty(value = "昵称")
    private String username;

    @ApiModelProperty(value = "性别 1 女，2 男")
    private Integer sex;

}
